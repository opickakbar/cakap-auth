package com.technicaltest.cakap.auth.service.impl;

import com.technicaltest.cakap.auth.dto.Token;
import com.technicaltest.cakap.auth.dto.User;
import com.technicaltest.cakap.auth.repository.TokenDataRepository;
import com.technicaltest.cakap.auth.repository.UserRepository;
import com.technicaltest.cakap.auth.service.UserService;
import com.technicaltest.common.exception.AuthException;
import com.technicaltest.common.request.auth.LoginAndRegisterRequest;
import com.technicaltest.common.response.auth.LoginResponse;
import com.technicaltest.common.response.auth.RegisterResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Objects;
import java.util.UUID;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenDataRepository tokenDataRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public RegisterResponse registerUser(LoginAndRegisterRequest loginAndRegisterRequest) {
        String username = loginAndRegisterRequest.getUsername();
        String password = loginAndRegisterRequest.getPassword();
        User user = userRepository.findByUsername(username);
        if (Objects.nonNull(user)) {
            throw new AuthException("User already exist!");
        }
        userRepository.save(User.builder()
                .username(username)
                .password(passwordEncoder.encode(password))
                .role("member")
                .build());
        return RegisterResponse.builder()
                .status(true)
                .message("Register success, please login to continue.")
                .build();
    }

    @Override
    public LoginResponse loginUser(LoginAndRegisterRequest loginAndRegisterRequest) {
        try {
            String username = loginAndRegisterRequest.getUsername();
            String password = loginAndRegisterRequest.getPassword();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password, Collections.emptyList()));
            User user = userRepository.findByUsername(username);
            if (Objects.isNull(user)) {
                throw new AuthException("User is invalid");
            }
            String token = UUID.randomUUID().toString().replaceAll("-", "");
            tokenDataRepository.save(Token.builder().token(token).user(user).build());
            return LoginResponse.builder()
                    .status(true)
                    .token(token)
                    .build();
        } catch (AuthenticationException e) {
            throw new AuthException("Authentication is invalid");
        }
    }
}
