package com.technicaltest.cakap.auth.service;

import com.technicaltest.common.request.auth.LoginAndRegisterRequest;
import com.technicaltest.common.response.auth.LoginResponse;
import com.technicaltest.common.response.auth.RegisterResponse;

public interface UserService {
    RegisterResponse registerUser(LoginAndRegisterRequest loginAndRegisterRequest);
    LoginResponse loginUser(LoginAndRegisterRequest loginAndRegisterRequest);
}
