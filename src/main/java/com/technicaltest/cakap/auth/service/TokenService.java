package com.technicaltest.cakap.auth.service;

import com.technicaltest.common.response.auth.CheckTokenResponse;

public interface TokenService {
    CheckTokenResponse checkToken (String token);
}
