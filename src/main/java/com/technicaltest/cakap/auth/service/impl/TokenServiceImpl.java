package com.technicaltest.cakap.auth.service.impl;

import com.technicaltest.cakap.auth.dto.Token;
import com.technicaltest.cakap.auth.dto.User;
import com.technicaltest.cakap.auth.repository.TokenDataRepository;
import com.technicaltest.cakap.auth.service.TokenService;
import com.technicaltest.common.exception.CommonException;
import com.technicaltest.common.response.auth.CheckTokenResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
public class TokenServiceImpl implements TokenService {

    @Autowired
    private TokenDataRepository tokenDataRepository;

    @Override
    public CheckTokenResponse checkToken(String token) {
        Token tokenData = tokenDataRepository.findByToken(token);
        if (Objects.isNull(tokenData)) {
            throw new CommonException("Token is invalid!");
        }
        return CheckTokenResponse.builder()
                .username(Optional.of(tokenData)
                        .map(Token::getUser)
                        .map(User::getUsername)
                        .orElse(null))
                .status(true)
                .message("Success check token data")
                .role(Optional.of(tokenData)
                        .map(Token::getUser)
                        .map(User::getRole)
                        .orElse(null))
                .build();
    }
}
