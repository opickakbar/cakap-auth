package com.technicaltest.cakap.auth.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.technicaltest.common.exception.AuthException;
import com.technicaltest.common.exception.CommonException;
import com.technicaltest.common.exception.ValidationException;
import com.technicaltest.common.response.ErrorResponse;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serializable;

@ControllerAdvice
@Slf4j
public class ControllerExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorResponse internalServerError(Exception exception) {
        log.error("InternalServerError at {} with Exception " + exception.getCause(), exception.getMessage());
        return ErrorResponse.builder()
                .status(false)
                .responseMessage(exception.getMessage())
                .build();
    }

    @ExceptionHandler(value = CommonException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse commonException(CommonException exception) {
        log.error("CommonException at {} with Exception " + exception.getCause(), exception.getMessage());
        return ErrorResponse.builder()
                .status(false)
                .responseMessage(exception.getMessage())
                .build();
    }

    @ExceptionHandler(value = AuthException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public ErrorResponse authException(AuthException exception) {
        log.error("AuthException at {} with Exception " + exception.getCause(), exception.getMessage());
        return ErrorResponse.builder()
                .status(false)
                .responseMessage(exception.getMessage())
                .build();
    }

    @ExceptionHandler(value = ValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse validationException(ValidationException exception) {
        log.error("ValidationException at {} with Exception " + exception.getCause(), exception.getMessage());
        return ErrorResponse.builder()
                .status(false)
                .responseMessage(exception.getMessage())
                .build();
    }
}
