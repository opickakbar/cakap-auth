package com.technicaltest.cakap.auth.controller;

import com.technicaltest.cakap.auth.service.TokenService;
import com.technicaltest.cakap.auth.service.UserService;
import com.technicaltest.cakap.auth.util.RequestValidationHelper;
import com.technicaltest.common.exception.ValidationException;
import com.technicaltest.common.request.auth.LoginAndRegisterRequest;
import com.technicaltest.common.response.auth.CheckTokenResponse;
import com.technicaltest.common.response.auth.LoginResponse;
import com.technicaltest.common.response.auth.RegisterResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@Slf4j
public class AuthServiceController {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private UserService userService;

    @GetMapping("/api/check-token/{token}")
    public ResponseEntity<CheckTokenResponse> checkToken(@PathVariable String token) {
        if (Objects.isNull(token)) {
            throw new ValidationException("token is required");
        }
        CheckTokenResponse checkTokenResponse = tokenService.checkToken(token);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(checkTokenResponse);
    }

    @PostMapping("/api/register")
    public ResponseEntity<RegisterResponse> register(@RequestBody LoginAndRegisterRequest loginAndRegisterRequest) {
        RequestValidationHelper.validateRequest(loginAndRegisterRequest);
        RegisterResponse registerResponse = userService.registerUser(loginAndRegisterRequest);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(registerResponse);
    }

    @PostMapping("/api/login")
    public ResponseEntity<LoginResponse> login(@RequestBody LoginAndRegisterRequest loginAndRegisterRequest) {
        RequestValidationHelper.validateRequest(loginAndRegisterRequest);
        LoginResponse loginResponse = userService.loginUser(loginAndRegisterRequest);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(loginResponse);
    }

}
