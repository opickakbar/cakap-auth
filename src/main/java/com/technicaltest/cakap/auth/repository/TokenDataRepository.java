package com.technicaltest.cakap.auth.repository;

import com.technicaltest.cakap.auth.dto.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenDataRepository extends JpaRepository<Token, Long> {
    Token findByToken(String token);
}
