package com.technicaltest.cakap.auth.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Builder
@Table(name = "tokens")
@NoArgsConstructor
@AllArgsConstructor
public class Token implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "token_generator")
    @SequenceGenerator(
            name = "token_generator",
            sequenceName = "token_sequence",
            initialValue = 1000
    )
    private Long id;

    @Column(name = "token", columnDefinition = "text")
    private String token;

    @CreatedDate
    @Column(name = "created_at")
    @JsonIgnore
    private ZonedDateTime createdAt = ZonedDateTime.now();

    @LastModifiedDate
    @Column(name = "updated_at")
    @JsonIgnore
    private ZonedDateTime updatedAt = ZonedDateTime.now();

    @Column(name = "expired_at")
    @JsonIgnore
    private ZonedDateTime expiredAt;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

}
