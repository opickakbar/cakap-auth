package com.technicaltest.cakap.auth.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Builder
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "user_generator")
    @SequenceGenerator(
            name = "user_generator",
            sequenceName = "user_sequence",
            initialValue = 1000
    )
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password", columnDefinition = "text")
    private String password;

    @Column(name = "role")
    private String role;

    @CreatedDate
    @Column(name = "created_at")
    @JsonIgnore
    private ZonedDateTime createdAt = ZonedDateTime.now();

    @LastModifiedDate
    @Column(name = "updated_at")
    @JsonIgnore
    private ZonedDateTime updatedAt = ZonedDateTime.now();
}
