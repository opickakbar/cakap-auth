package com.technicaltest.cakap.auth.util;

import com.technicaltest.common.exception.ValidationException;
import com.technicaltest.common.request.auth.LoginAndRegisterRequest;

import java.util.Optional;

public class RequestValidationHelper {
    private RequestValidationHelper() {
    }

    public static void validateRequest(LoginAndRegisterRequest loginAndRegisterRequest) {
        String username = Optional.ofNullable(loginAndRegisterRequest)
                .map(LoginAndRegisterRequest::getUsername)
                .orElseGet(String::new);
        String password = Optional.ofNullable(loginAndRegisterRequest)
                .map(LoginAndRegisterRequest::getPassword)
                .orElseGet(String::new);
        if (username.isEmpty()) {
            throw new ValidationException("username is required!");
        } else if (password.isEmpty()) {
            throw new ValidationException("password is required");
        }
    }
}
