package com.technicaltest.cakap.auth.variables;

import com.technicaltest.cakap.auth.dto.Token;
import com.technicaltest.cakap.auth.dto.User;
import com.technicaltest.common.response.auth.CheckTokenResponse;

public class TokenServiceImplTestVariable {

    public static String VALID_TOKEN = "asdasdasdasdasdqwewqeq";

    public static Token VALID_TOKEN_RESPONSE = Token.builder()
            .token(VALID_TOKEN)
            .id(Long.valueOf("1"))
            .user(User.builder()
                    .username("taufikakbar")
                    .password("sjsadasdsjadhsabdasdsadsadasfgergergreg")
                    .role("admin")
                    .build())
            .build();

    public static CheckTokenResponse CHECK_TOKEN_VALID_RESPONSE = CheckTokenResponse.builder()
            .username("taufikakbar")
            .status(true)
            .message("Success check token data")
            .role("admin")
            .build();
}
