package com.technicaltest.cakap.auth.variables;

import com.technicaltest.common.request.auth.LoginAndRegisterRequest;
import com.technicaltest.common.response.auth.RegisterResponse;

public class UserServiceImplTestVariable {

    public static LoginAndRegisterRequest REGISTER_REQUEST_VALID = LoginAndRegisterRequest.builder()
            .username("taufikakbar")
            .password("test123")
            .build();

    public static RegisterResponse REGISTER_RESPONSE_VALID = RegisterResponse.builder()
            .status(true)
            .message("Register success, please login to continue.")
            .build();
}
