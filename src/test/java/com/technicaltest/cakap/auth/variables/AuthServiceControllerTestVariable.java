package com.technicaltest.cakap.auth.variables;

import com.technicaltest.common.request.auth.LoginAndRegisterRequest;
import com.technicaltest.common.response.auth.LoginResponse;
import com.technicaltest.common.response.auth.RegisterResponse;

public class AuthServiceControllerTestVariable {

    public static LoginAndRegisterRequest LOGIN_REQUEST_VALID = LoginAndRegisterRequest.builder()
            .username("TAUFIKAKBAR")
            .password("test123")
            .build();

    public static LoginResponse LOGIN_RESPONSE_VALID = LoginResponse.builder()
            .status(true)
            .token("asjdhgajshgdsadguwgdjqhwgdasd")
            .build();

    public static RegisterResponse REGISTER_RESPONSE_VALID = RegisterResponse.builder()
            .status(true)
            .message("register success")
            .build();

    public static LoginAndRegisterRequest LOGIN_REQUEST_INVALID = LoginAndRegisterRequest.builder()
            .password("test123")
            .build();
}
