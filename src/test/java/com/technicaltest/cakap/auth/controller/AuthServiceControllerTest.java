package com.technicaltest.cakap.auth.controller;

import com.technicaltest.cakap.auth.service.TokenService;
import com.technicaltest.cakap.auth.service.UserService;
import com.technicaltest.cakap.auth.variables.AuthServiceControllerTestVariable;
import com.technicaltest.common.exception.ValidationException;
import com.technicaltest.common.response.auth.LoginResponse;
import com.technicaltest.common.response.auth.RegisterResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class AuthServiceControllerTest extends AuthServiceControllerTestVariable {

    @InjectMocks
    private AuthServiceController authServiceController;

    @Mock
    private UserService userService;

    @Mock
    private TokenService tokenService;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    public void tearDown() throws Exception {
        Mockito.verifyNoMoreInteractions(userService);
        Mockito.verifyNoMoreInteractions(tokenService);
    }

    @Test
    void login_whenAllValid_thenShouldReturnSuccess () {
        Mockito.when(userService.loginUser(LOGIN_REQUEST_VALID)).thenReturn(LOGIN_RESPONSE_VALID);
        ResponseEntity<LoginResponse> actual = authServiceController.login(LOGIN_REQUEST_VALID);
        Mockito.verify(userService).loginUser(LOGIN_REQUEST_VALID);
        Assertions.assertEquals(HttpStatus.OK, actual.getStatusCode());
        Assertions.assertEquals(LOGIN_RESPONSE_VALID, actual.getBody());
    }

    @Test
    void login_whenUsernameIsMissing_thenShouldReturnError () {
        try {
            ResponseEntity<LoginResponse> actual = authServiceController.login(LOGIN_REQUEST_INVALID);
        } catch (ValidationException exception) {
            Assertions.assertEquals("username is required!", exception.getMessage());
        }
    }

    @Test
    void register_whenAllValid_thenShouldReturnSuccess () {
        Mockito.when(userService.registerUser(LOGIN_REQUEST_VALID)).thenReturn(REGISTER_RESPONSE_VALID);
        ResponseEntity<RegisterResponse> actual = authServiceController.register(LOGIN_REQUEST_VALID);
        Mockito.verify(userService).registerUser(LOGIN_REQUEST_VALID);
        Assertions.assertEquals(HttpStatus.OK, actual.getStatusCode());
        Assertions.assertEquals(REGISTER_RESPONSE_VALID, actual.getBody());
    }

    @Test
    void register_whenUsernameIsMissing_thenShouldReturnError () {
        try {
            ResponseEntity<RegisterResponse> actual = authServiceController.register(LOGIN_REQUEST_INVALID);
        } catch (ValidationException exception) {
            Assertions.assertEquals("username is required!", exception.getMessage());
        }
    }


}
