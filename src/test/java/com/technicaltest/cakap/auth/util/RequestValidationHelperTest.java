package com.technicaltest.cakap.auth.util;

import com.technicaltest.common.exception.ValidationException;
import com.technicaltest.common.request.auth.LoginAndRegisterRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RequestValidationHelperTest {

    @Test
    void validateRequestValidTest () {
        LoginAndRegisterRequest loginAndRegisterRequest = LoginAndRegisterRequest.builder()
                .username("test-username")
                .password("test-password")
                .build();
        RequestValidationHelper.validateRequest(loginAndRegisterRequest);
    }

    @Test
    void validateRequestMissingUsernameTest () {
        LoginAndRegisterRequest loginAndRegisterRequest = LoginAndRegisterRequest.builder()
                .password("test-password")
                .build();
        try {
            RequestValidationHelper.validateRequest(loginAndRegisterRequest);
        } catch (ValidationException exception) {
            Assertions.assertEquals("username is required!", exception.getMessage());
        }
    }

    @Test
    void validateRequestMissingPasswordTest () {
        LoginAndRegisterRequest loginAndRegisterRequest = LoginAndRegisterRequest.builder()
                .username("test-username")
                .build();
        try {
            RequestValidationHelper.validateRequest(loginAndRegisterRequest);
        } catch (ValidationException exception) {
            Assertions.assertEquals("password is required", exception.getMessage());
        }
    }
}
