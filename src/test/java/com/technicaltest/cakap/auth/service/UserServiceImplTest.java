package com.technicaltest.cakap.auth.service;

import com.technicaltest.cakap.auth.dto.Token;
import com.technicaltest.cakap.auth.dto.User;
import com.technicaltest.cakap.auth.repository.TokenDataRepository;
import com.technicaltest.cakap.auth.repository.UserRepository;
import com.technicaltest.cakap.auth.service.impl.TokenServiceImpl;
import com.technicaltest.cakap.auth.service.impl.UserServiceImpl;
import com.technicaltest.cakap.auth.variables.UserServiceImplTestVariable;
import com.technicaltest.common.exception.CommonException;
import com.technicaltest.common.request.auth.LoginAndRegisterRequest;
import com.technicaltest.common.response.auth.CheckTokenResponse;
import com.technicaltest.common.response.auth.LoginResponse;
import com.technicaltest.common.response.auth.RegisterResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;

public class UserServiceImplTest extends UserServiceImplTestVariable {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TokenDataRepository tokenRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private AuthenticationManager authenticationManager;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    public void tearDown() throws Exception {
        Mockito.verifyNoMoreInteractions(userRepository);
        Mockito.verifyNoMoreInteractions(passwordEncoder);
        Mockito.verifyNoMoreInteractions(authenticationManager);
    }

    @Test
    void registerUser_whenAllValid_thenShouldReturnSuccess () {
        Mockito.when(userRepository.findByUsername("taufikakbar")).thenReturn(null);
        Mockito.when(passwordEncoder.encode("test123")).thenReturn("generated");
        RegisterResponse actual = userService.registerUser(REGISTER_REQUEST_VALID);
        Assertions.assertEquals(REGISTER_RESPONSE_VALID, actual);
        Mockito.verify(userRepository).findByUsername("taufikakbar");
        Mockito.verify(userRepository).save(User.builder()
                .password("generated")
                .role("member")
                .username("taufikakbar")
                .build());
        Mockito.verify(passwordEncoder).encode("test123");
    }

    @Test
    void loginUser_whenAllValid_thenShouldReturnSuccess () {
        Mockito.when(authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("taufikakbar", "test123", Collections.emptyList())))
                .thenReturn(null);
        Mockito.when(userRepository.findByUsername("taufikakbar")).thenReturn(User.builder()
                .username("taufikakbar")
                .role("member")
                .password("asdjsabdhbasjdbsadasdsawefewfwef")
                .id(Long.valueOf("1"))
                .build());
        Mockito.when(tokenRepository.save(Mockito.any())).thenReturn(Token.builder()
                .user(User.builder()
                        .username("taufikakbar")
                        .role("member")
                        .password("asdjsabdhbasjdbsadasdsawefewfwef")
                        .id(Long.valueOf("1"))
                        .build())
                .token("token_generated_test")
                .build());
        LoginResponse actual = userService.loginUser(LoginAndRegisterRequest.builder()
                .username("taufikakbar")
                .password("test123")
                .build());
        Assertions.assertEquals(true, actual.getStatus());
        Mockito.verify(authenticationManager).authenticate(new UsernamePasswordAuthenticationToken("taufikakbar", "test123", Collections.emptyList()));
        Mockito.verify(userRepository).findByUsername("taufikakbar");
        Mockito.verify(tokenRepository).save(Mockito.any());
    }
}
