package com.technicaltest.cakap.auth.service;

import com.technicaltest.cakap.auth.repository.TokenDataRepository;
import com.technicaltest.cakap.auth.service.impl.TokenServiceImpl;
import com.technicaltest.cakap.auth.variables.TokenServiceImplTestVariable;
import com.technicaltest.common.exception.CommonException;
import com.technicaltest.common.response.auth.CheckTokenResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class TokenServiceImplTest extends TokenServiceImplTestVariable {

    @InjectMocks
    private TokenServiceImpl tokenService;

    @Mock
    private TokenDataRepository tokenDataRepository;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    public void tearDown() throws Exception {
        Mockito.verifyNoMoreInteractions(tokenDataRepository);
    }

    @Test
    void checkToken_whenAllValid_thenShouldReturnSuccess () {
        Mockito.when(tokenDataRepository.findByToken(VALID_TOKEN)).thenReturn(VALID_TOKEN_RESPONSE);
        CheckTokenResponse actual = tokenService.checkToken(VALID_TOKEN);
        Assertions.assertEquals(CHECK_TOKEN_VALID_RESPONSE, actual);
        Mockito.verify(tokenDataRepository).findByToken(VALID_TOKEN);
    }

    @Test
    void checkToken_whenInvalidToken_thenShouldReturnError () {
        Mockito.when(tokenDataRepository.findByToken("this_is_invalid_token")).thenReturn(null);
        try {
            CheckTokenResponse actual = tokenService.checkToken("this_is_invalid_token");
        } catch (CommonException exception) {
            Assertions.assertEquals("Token is invalid!", exception.getMessage());
            Mockito.verify(tokenDataRepository).findByToken("this_is_invalid_token");
        }
    }

}
